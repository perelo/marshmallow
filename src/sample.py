#!/usr/bin/env python
# -*- coding: utf-8 -*-

__date__ = '22-11-2013'

from numpy import *
import random
import pylab as pl
from itertools import cycle

sample_xor = array([[0,0],[0,1],[1,0],[1,1]])
target_xor = array([   -1,    1,    1,   -1])

sample = array([ [3.5, 0.5], [2.5, 2.0], [4.5, 1.5], [5.0, 2.5], [6.0, 4.0],
                 [2.5, 3.5], [1.0, 4.0], [2.0, 6.5], [4.0, 5.5] ])
target = array([-1,-1,-1,-1,-1,1,1,1,1])

t_sample = array([ [2.0, 3.0], [0.0, 5.0], [4.5, 5.5], [3.0, 6.0], [7.0, 6.5],
                   [0.5, 2.0], [1.5, 2.0], [2.5, 1.0], [4.5, 3.5], [6.5, 3.0],
                   [7.0, 5.5] ])
t_target = array([1,1,1,1,1,-1,-1,-1,-1,-1,-1])

def produce_first_sample(nbfeatures, nsamples, tbc=0):
    s=empty((nsamples, nbfeatures))
    y=ones((nsamples), dtype=int16)
    coeff=arange(nbfeatures)
    for i in range(nbfeatures):
        coeff[i]=random.uniform(-100, 100)
    for ex in range(nsamples):
        for i in range(nbfeatures):
            a = random.uniform(-10, 10)
            b = random.uniform(-8, 8)
            x = random.uniform(a, b)
            s[ex, i]=x
        c=0.
        for j in range(nbfeatures):
            c=c+coeff[j]*pow(s[ex, j], j)
        if (c >coeff[0]):
            y[ex]=-1
    ex=0
    while (ex<nsamples*tbc/100):
        nc = random.randint(0, 1)
        if (nc==0):
            nc=-1
        ni = random.randint(0, nsamples-1)
        if (nc <> y[ni]):
            y[ni]=nc
            ex = ex+1
    return [s, y]

def produce_second_sample(nbfeatures, nsamples, tbc=0):
    s=empty((nsamples, nbfeatures))
    y=ones((nsamples), dtype=int16)
    iex=0
    while (iex < nsamples):
        ife = 0
        while (ife < nbfeatures):
            delta = random.randint(10, 15)
            xife0 = random.uniform(-10.,  10.)
            xife1 = random.uniform(-10.+delta,  10.+delta)
            s[iex, ife]=xife0
            if (iex+1 <> nsamples):
                s[iex+1, ife]=xife1
                y[iex+1]=-1
            ife = ife+1
        iex = iex+2
        ex=0
    while (ex<nsamples*tbc/100):
        nc = random.randint(0, 1)
        if (nc==0):
            nc=-1
        ni = random.randint(0, nsamples-1)
        if (nc <> y[ni]):
            y[ni]=nc
            ex = ex+1
    return [s, y]
