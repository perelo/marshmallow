#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Eloi Perdereau'
__date__ = '22-11-2013'

from itertools import cycle
from collections import defaultdict
import numpy as np
from math import exp
from sklearn import cross_validation as cv

def signum(x):
    return -1 if x < 0 else 1

def gaussian_kernel(v1, v2, sigma):
    num = np.power(np.linalg.norm(v1-v2), 2)
    den = np.power(sigma, 2)
    return exp(- num / den)

def polynomial_kernel(v1, v2, k):
    return np.power(1 + np.dot(v1, v2), k)

def compute_gram(data, kernel):
    array = []
    for vi in data:
        for vj in data:
            array.append(kernel(vi,vj))
    return np.reshape(array, (len(data), -1))

def learn_kernel_perceptron(data, target, kernel, h):

    a = np.zeros(len(data))   # alphas
    gram = compute_gram(data, lambda v1, v2: kernel(v1, v2, h))

    MAX_ITER = 1000
    nb_iter = 0     # total amount of iterations
    nb_ok = 0       # nb of consecutive learning success
    for i in cycle(xrange(len(data))):
        # test if we have cycled concecutively through all targets w/o errors
        if nb_ok >= len(target):
            break

        # test if the learning is too long
        if nb_iter >= MAX_ITER:
            print 'Learning too long, stop after ', nb_iter, ' iterations.'
            break
        nb_iter += 1

        # compute the damn thing
        res = target[i] * sum(a * target * gram[i])

        if res <= 0:    # learning error
            a[i] += 1
            nb_ok = 0
        else:           # learning success
            nb_ok += 1

    return a

def predict_kernel_perceptron(data, target, a, x, kernel, h):
    res = 0
    for i in xrange(len(a)):
        res += a[i] * target[i] * kernel(x, data[i], h)
    return signum(res)

def estimate_kernel_perceptron(dA, tA, dT, tT, kernel, h):
    a = learn_kernel_perceptron(dA, tA, kernel, h)
    nb_errT = 0
    # iterate through the data, predict and count errors
    for i in xrange(len(dT)):
        if predict_kernel_perceptron(dA, tA, a, dT[i], kernel, h) != tT[i]:
            nb_errT += 1
    # return the error ratio
    return (1.0 * nb_errT) / len(dT)

def best_hyperparameter(data, target, kernel):
    best = 0
    min_err = 1
    dA, dT, tA, tT = cv.train_test_split(data, target)
    for h in xrange(1, 20):
        err = estimate_kernel_perceptron(dA, tA, dT, tT, kernel, h)
        # print h, err
        if err < min_err:
            best = h
            min_err = err
    best_hyperparameter.min_err = min_err
    return best
