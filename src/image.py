#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Eloi Perdereau'
__date__ = '10-12-2013'

from PIL import Image
import numpy as np
import os

def vect_matrix_from_dir(directory):
    sz = (32, 32)
    length = sz[0] * sz[1]
    mat = []
    for f in os.listdir(directory):
        fname = directory + '/' + f
        im = Image.open(fname).convert('L').resize(sz)
        vect = np.reshape(np.array(im), (1, length))
        mat.append(vect.flatten())
    return mat

def hist_matrix_from_dir(directory):
    mat = []
    for f in os.listdir(directory):
        fname = directory + '/' + f
        im = Image.open(fname).convert('L')
        width, height = im.size
        mat.append(np.array(im.histogram()) / (1.0 * width * height))
    return mat

def import_image_vect():
    return np.load("../data/images.npy")

def import_image_hist():
    return np.load("../data/histograms.npy")
