#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Eloi Perdereau'
__date__ = '10-12-2013'

import sys
sys.path.append('../src')

from kernel import *
from image import *
from sample import *

def get_data_target(matrix_from_dir):
    bowl = matrix_from_dir('../data/Bowling')
    mars = matrix_from_dir('../data/Mars')

    data = bowl + mars
    target = ([-1] * len(bowl)) + ([1] * len(mars))

    return data, target

def test_image_best_hyperparameter():
    for matrix_from_dir in [vect_matrix_from_dir, hist_matrix_from_dir]:
        print '=>', matrix_from_dir.__name__
        data, target = get_data_target(matrix_from_dir)

        for kernel in [gaussian_kernel, polynomial_kernel]:
            best_hypp = best_hyperparameter(data, target, kernel)
            print 'kernel =', kernel.__name__, \
                  'best =', best_hypp, \
                  'err =', best_hyperparameter.min_err

def test_prediction():
    v, h = 'vectors', 'histograms'

    best_configurations = [
        [v, gaussian_kernel,   12],
        [v, polynomial_kernel,  5],
        [h, gaussian_kernel,    3],
        [h, polynomial_kernel,  5]
        ]

    rep_fcts = { v : [vect_matrix_from_dir, import_image_vect],
                 h : [hist_matrix_from_dir, import_image_hist] }

    for rep, kernel, h in best_configurations:
        matrix_from_dir, import_image = rep_fcts[rep]

        print '=>', rep, 'with', kernel.__name__, h

        dT, tT = get_data_target(matrix_from_dir)
        pcp = learn_kernel_perceptron(dT, tT, kernel, h)

        data = import_image()
        predicted_classes = []
        for d in data:
            d_class = predict_kernel_perceptron(dT, tT, pcp, d, kernel, h)
            predicted_classes.append(d_class)

        print '\t', predicted_classes

if __name__ == '__main__':
    # test_image_best_hyperparameter()
    test_prediction()
