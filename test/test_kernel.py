#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Eloi Perdereau'
__date__ = '22-11-2013'

import sys
sys.path.append('../src')

import numpy as np
from kernel import *
from sample import *

def test_mini_kernel_perceptron():
    print 'Testing kernel perceptron with small smaple'
    dA, tA = sample, target
    dT, tT = t_sample, t_target
    kernel, h = gaussian_kernel, 1
    a = learn_kernel_perceptron(dA, tA, kernel, h)

    print '\talpha = ', a
    print '\terror = ', estimate_kernel_perceptron(dA, tA, dT, tT, kernel, h)

def test_scale_kernel_perceptron():
    nf = 400
    ne = 100
    kernel, h = gaussian_kernel, 2

    for produce_sample in (produce_first_sample, produce_second_sample):
        print 'produce_sample = {}()'.format(produce_sample.__name__)
        for tbc in [0, 5, 10, 30]:
            dA, tA = produce_sample(nf, ne, tbc)
            dT, tT = produce_sample(nf, ne)
            err_pcp = estimate_kernel_perceptron(dA, tA, dT, tT, kernel, h)
            print '\ttbc = {}, \terr = {}'.format(tbc, err_pcp)

def test_best_hyperparameter():
    print 'Testing best hyperparameter computation with small sample'
    for kernel in [gaussian_kernel, polynomial_kernel]:
        best_hypp = best_hyperparameter(sample, target, kernel)
        print '  kernel =', kernel.__name__, \
              '\tbest hyperparameter =', best_hypp, \
              '\tmin_err =', best_hyperparameter.min_err


if __name__ == '__main__':
    # test_mini_kernel_perceptron()
    test_scale_kernel_perceptron()
